% Preditor-prey population estimation using explicit euler's method
% Author: Aryal Bibek

% Define initial parameters
h = 0.0125;             % Timestep size
cur_x = 1/2;            % Initial x (Current value of x)
cur_y = 1;              % Initial y (Current value of y)
cur_z = 2;              % Initial z (Current value of z)
n = 1000;               % Total number of steps
X(1) = cur_x;           % Array to store x at each iteration
Y(1) = cur_y;           % Array to store y at each iteration
Z(1) = cur_z;           % Array to store z at each iteration

for i = 2:n
    % Calculate new values of x,y,z
    new_x = cur_x + h*(cur_x-cur_x*cur_y);
    new_y = cur_y + h*(-cur_y+cur_x*cur_y-cur_y*cur_z);
    new_z = cur_z + h*(-cur_z+cur_y*cur_z);
    % Update the current values with new values for next iteration
    cur_x = new_x;
    cur_y = new_y;
    cur_z = new_z;
    % Store the obtained value in array
    X(i) = cur_x;
    Y(i) = cur_y;
    Z(i) = cur_z;
end

hold on;
plot(X,'b-');
plot(Y,'g-');
plot(Z,'r-');
xlabel('TimeStep');
ylabel('Population');
legend('Population of Species(x)','Population of Species(y)','Population of Species(z)');
title("Preditor-Prey population estimation");
hold off;


